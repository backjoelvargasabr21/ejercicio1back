package com.techu.ejercicio1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class Ejercicio1Controller {
    private ArrayList<Producto> listaProductos = null;

    public Ejercicio1Controller() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1, "PR1",27.35));
        listaProductos.add(new Producto(2, "PR2", 18.33));
    }

    /* Get lista por ID de productos */
    @GetMapping("/ejercicio1/productos/{id}")
    public ResponseEntity<Producto> obtenerProductoPorId(@PathVariable int id){
        Producto resultado = null;
        ResponseEntity<Producto> respuesta = null;
        try{
            resultado = listaProductos.get(id-1);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex){
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /* Add nuevo producto */
    @PostMapping(value = "/ejercicio1/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody Producto productoNuevo) {
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /* Actualizar Producto*/
    @PutMapping("/ejercicio1/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id, @RequestBody Producto ProductoUpdate)
    {
        ResponseEntity<String> resultado = null;
        try{
            Producto productoAModificar = listaProductos.get(id);
            productoAModificar.setNombre(ProductoUpdate.getNombre());
            productoAModificar.setPrecio(ProductoUpdate.getPrecio());
            listaProductos.set(id, productoAModificar);
            resultado = new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception ex){
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /* Eliminar Producto por ID */
    @DeleteMapping("/ejercicio1/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try{
            Producto productoAEliminar = listaProductos.get(id-1);
            listaProductos.remove(id-1);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /* Actualizar parcial un producto*/
    @PatchMapping("/ejercicio1/productos/{id}")
    public ResponseEntity patchPrecioProducto(@RequestBody Producto productoPatch, @PathVariable int id){

        ResponseEntity<String> resultado = null;
        try{
            Producto ProductoP = listaProductos.get(id-1);
            ProductoP.setPrecio(productoPatch.getPrecio());
            listaProductos.set(id-1,ProductoP);
            resultado = new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception ex){
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /* Obtener el subrecurso del nombre por ID*/
    @GetMapping("/ejercicio1/productos/{id}/nombre")
    public ResponseEntity<Producto> obtenerNombrePorId(@PathVariable int id) {
        Producto resultado = null;
        String nombre = null;
        ResponseEntity<Producto> respuesta = null;
        try {
            resultado = listaProductos.get(id - 1);
            resultado.getNombre();
            respuesta = new ResponseEntity(resultado.getNombre(), HttpStatus.OK);
        } catch (Exception ex) {
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

}